package eu.pijusn.talk.dependency_injection.b_property

import org.scalatest.FlatSpec

class UsersServiceTest extends FlatSpec {
  "Creating user" should "accept new user" in {
    val service = new UsersService
    val validator = new Validator

    service.validator = validator

    val userWithInvalidEmail = User("user@wix.com", "password")
    service.createUser(userWithInvalidEmail)
  }

  it should "validate email" in {
    val service = new UsersService
    val validator = new Validator

    service.validator = validator

    val userWithInvalidEmail = User("bad-bad-email", "password")
    assertThrows[IllegalArgumentException](service.createUser(userWithInvalidEmail))
  }

  it should "validate password" in {
    val service = new UsersService
    val validator = new Validator

    service.validator = validator

    val userWithInvalidEmail = User("user@wix.com", "!#$%^&*")
    assertThrows[IllegalArgumentException](service.createUser(userWithInvalidEmail))
  }
}
