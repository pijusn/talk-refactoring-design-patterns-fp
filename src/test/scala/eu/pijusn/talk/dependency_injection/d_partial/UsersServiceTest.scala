package eu.pijusn.talk.dependency_injection.d_partial

import org.scalatest.FlatSpec

class UsersServiceTest extends FlatSpec {
  "Creating user" should "accept new user" in {
    val validator = new Validator
    val createUser = UsersService.createUser(validator) _

    val userWithInvalidEmail = User("user@wix.com", "password")
    createUser(userWithInvalidEmail)
  }

  it should "validate email" in {
    val validator = new Validator
    val createUser = UsersService.createUser(validator) _

    val userWithInvalidEmail = User("bad-bad-email", "password")
    assertThrows[IllegalArgumentException](createUser(userWithInvalidEmail))
  }

  it should "validate password" in {
    val validator = new Validator
    val createUser = UsersService.createUser(validator) _

    val userWithInvalidEmail = User("user@wix.com", "!#$%^&*")
    assertThrows[IllegalArgumentException](createUser(userWithInvalidEmail))
  }
}
