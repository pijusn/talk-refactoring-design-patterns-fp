package eu.pijusn.talk.factory_method.b_declarative

import java.io.{File, FileReader, InputStreamReader}
import java.net.{URI, URL}

import com.google.common.io.CharStreams

trait DataFile {
  def content: String
}

object DataFile {
  def apply(uri: String): DataFile = apply(new URI(uri))

  def apply(uri: URI): DataFile = {
    uri.getScheme match {
      case "https" => new RemoteFile(uri.getHost, uri.getPath)
      case "classpath" => new ResourceFile(uri.getPath)
      case scheme => throw new UnsupportedOperationException(s"scheme=$scheme")
    }
  }
}

class RemoteFile(host: String, path: String) extends DataFile {
  override def content: String = {
    val url = new URL("https", host, path).openConnection().getInputStream
    val reader = new InputStreamReader(url)
    CharStreams.toString(reader)
  }
}

class ResourceFile(path: String) extends DataFile {
  override def content: String = {
    val file = new File(getClass.getResource(path).getFile)
    val reader = new FileReader(file)
    CharStreams.toString(reader)
  }
}
