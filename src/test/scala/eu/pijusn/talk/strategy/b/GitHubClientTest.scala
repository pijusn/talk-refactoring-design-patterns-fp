package eu.pijusn.talk.strategy.b

import org.scalatest.FlatSpec

class GitHubClientTest extends FlatSpec {
  "Getting file" should "return file content as string" in {
    val client = new GitHubClient(new ApacheHttpClient)
    val content = client.getFile("wix", "http-testkit", "master", "/README.md")
    assert(content contains "EmbeddedHttpProbe")
  }
}
