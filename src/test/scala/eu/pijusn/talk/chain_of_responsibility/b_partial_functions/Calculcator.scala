package eu.pijusn.talk.chain_of_responsibility.b_partial_functions

import eu.pijusn.talk.chain_of_responsibility.b_partial_functions.Calculator._
import org.scalatest.FlatSpec

sealed trait Operation
case class Addition(a: Int, b: Int) extends Operation
case class Subtraction(a: Int, b: Int) extends Operation
case class Multiplication(a: Int, b: Int) extends Operation
case class Division(a: Int, b: Int) extends Operation

object Calculator {
  val add: PartialFunction[Operation, Int] = {
    case Addition(left, right) => left + right
  }

  val subtract: PartialFunction[Operation, Int] = {
    case Subtraction(left, right) => left - right
  }

  val multiply: PartialFunction[Operation, Int] = {
    case Multiplication(left, right) => left * right
  }

  val divide: PartialFunction[Operation, Int] = {
    case Division(left, right) => left / right
  }
}

class CalculatorTest extends FlatSpec {
  "Calculator" should "add" in {
    val result = compute(Addition(1, 3))
    assert(4 === result)
  }

  it should "subtract" in {
    val result = compute(Subtraction(1, 3))
    assert(-2 === result)
  }

  it should "multiply" in {
    val result = compute(Multiplication(1, 3))
    assert(3 === result)
  }

  it should "divide" in {
    val result = compute(Division(1, 3))
    assert(0 === result)
  }

  private def compute = {
    add orElse subtract orElse multiply orElse divide
  }
}
