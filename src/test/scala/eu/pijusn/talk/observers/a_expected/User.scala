package eu.pijusn.talk.observers.a_expected

import java.time.{Duration, Instant}

import org.mockito.Mockito.{verify, verifyZeroInteractions}
import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import org.scalatest.mockito.MockitoSugar._

trait UserObserver {
  def onLoggedIn(user: User, previousLogin: Instant)
}

// Mutable and open for extra side-effects.
class User(name: String, private var lastLogin: Instant) {
  private var observers = Seq.empty[UserObserver]

  def registerObserver(observer: UserObserver): Unit = synchronized {
    observers :+= observer
  }

  def getName: String = name

  def getLastLogin: Instant = synchronized {
    lastLogin
  }

  def setLastLogin(timestamp: Instant): Unit = synchronized {
    val previousLogin = lastLogin
    lastLogin = timestamp
    observers.foreach(_.onLoggedIn(this, previousLogin))
  }
}

class RecentLoginObserver(announcer: Announcer) extends UserObserver {
  override def onLoggedIn(user: User, previousLogin: Instant): Unit = {
    val timeDelta = Duration.between(previousLogin, user.getLastLogin)
    val hasLoggedInRecently = timeDelta.minusDays(7).isNegative
    if (!hasLoggedInRecently) {
      announcer.welcomeUser(user)
    }
  }
}

// Side-effects.
class Announcer {
  def welcomeUser(user: User): Unit = {
    println(s"Welcome back, ${user.getName}. We missed you!")
  }
}

class ServiceFacade {
  def login(user: User): Unit = {
    user.setLastLogin(Instant.now())
  }
}

class UserTest extends FlatSpec {
  "changing login timestamp" should "invoke observer" in {
    val oldTimestamp = Instant.ofEpochMilli(1400000000)
    val newTimestamp = Instant.ofEpochMilli(1500000000)
    val user = new User("user", oldTimestamp)

    var invoked = false
    user.registerObserver((_, _) => invoked = true)

    user.setLastLogin(newTimestamp)

    invoked should be(true)
  }

  it should "pass along old timestamp to the observer" in {
    val oldTimestamp = Instant.ofEpochMilli(1400000000)
    val newTimestamp = Instant.ofEpochMilli(1500000000)
    val user = new User("user", oldTimestamp)

    var timestampPassed: Instant = null
    user.registerObserver((_, timestamp) => timestampPassed = timestamp)

    user.setLastLogin(newTimestamp)

    timestampPassed should be(oldTimestamp)
  }
}

class RecentLoginObserverTest extends FlatSpec {
  "login observer" should "do nothing if user has recently logged in" in {
    val announcer = mock[Announcer]
    val observer = new RecentLoginObserver(announcer)

    val newTimestamp = Instant.ofEpochMilli(1400000000)
    val oldTimestamp = daysBefore(newTimestamp, 1)
    val user = new User("user", newTimestamp)

    observer.onLoggedIn(user, oldTimestamp)

    verifyZeroInteractions(announcer)
  }

  it should "invoke announcer if user hasn't logged in 14 days" in {
    val announcer = mock[Announcer]
    val observer = new RecentLoginObserver(announcer)

    val newTimestamp = Instant.ofEpochMilli(1400000000)
    val oldTimestamp = daysBefore(newTimestamp, 14)
    val user = new User("user", newTimestamp)

    observer.onLoggedIn(user, oldTimestamp)

    verify(announcer).welcomeUser(user)
  }

  private def daysBefore(instant: Instant, numberOfDays: Int) = instant.minus(Duration.ofDays(numberOfDays))
}



