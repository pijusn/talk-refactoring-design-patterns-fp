package eu.pijusn.talk.singleton.a_raw_imperative

import org.scalatest.FlatSpec

import scala.collection.mutable

class Factorial private {
  private val cache = new mutable.HashMap[Long, BigInt]

  def computeFactorial(n: Long): BigInt = cache.getOrElseUpdate(n, compute(n))

  private def compute(n: Long): BigInt = {
    var result = BigInt(1)
    for (x <- 1L to n) {
      result *= x
    }
    result
  }
}

object Factorial {
  private var instance: Factorial = _

  def getInstance(): Factorial = synchronized {
    if (instance == null) {
      instance = new Factorial
    }
    instance
  }
}


class FactorialTest extends FlatSpec {
  "Factorial" should "compute value of 5 factorial" in {
    val result = Factorial.getInstance().computeFactorial(5)
    assert(result == 120)
  }

  it should "compute value of 30 factorial" in {
    val result = Factorial.getInstance().computeFactorial(30)
    assert(result == BigInt("265252859812191058636308480000000"))
  }
}
