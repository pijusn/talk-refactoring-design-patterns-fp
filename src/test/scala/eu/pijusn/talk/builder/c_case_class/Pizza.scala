package eu.pijusn.talk.builder.c_case_class

import org.scalatest.FlatSpec

case class Pizza(name: String = "Margarita",
                 size: PizzaSize = Large,
                 toppings: Seq[String] = Seq.empty,
                 dough: Dough) {
  require(toppings.nonEmpty, "no toppings")
}

sealed trait Dough
case object Full extends Dough
case object White extends Dough
case object SourDough extends Dough

sealed trait PizzaSize
case object Small extends PizzaSize
case object Medium extends PizzaSize
case object Large extends PizzaSize

class PizzaTest extends FlatSpec {
  "Builder" should "construct pizza" in {
    val pizza = Pizza(
      name = "Hawaiian",
      size = Large,
      toppings = Seq("Anchovy", "Olives"),
      dough = Full
    )

    println(pizza)
  }

  it should "construct pizza dynamically" in {
    val isGift = Math.random() < 0.2
    var pizza = Pizza(
      name = "Pepperoni",
      toppings = Seq("Pepperoni", "Cheese", "Tomato Sauce"),
      dough = Full
    )

    if (isGift) pizza = pizza.copy(size = Large)

    println(pizza)
  }

  it should "not build pizza without toppings" in {
    def buildInvalidPizza = Pizza(
      name = "Hawaiian",
      size = Large,
      toppings = Seq.empty,
      dough = Full
    )

    assertThrows[IllegalStateException](buildInvalidPizza)
  }
}
