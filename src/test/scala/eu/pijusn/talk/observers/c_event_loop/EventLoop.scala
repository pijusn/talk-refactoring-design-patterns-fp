package eu.pijusn.talk.observers.c_event_loop

import scala.annotation.tailrec

class EventLoop(handlers: PartialFunction[Any, Seq[Any]]*) {
  def apply(event: Any): Unit = {
    process(Seq(event))
  }

  @tailrec
  private def process(events: Seq[Any]): Unit = {
    events match {
      case Nil =>
      case event :: remainingEvents =>
        val newEvents = handlers.flatMap(_.applyOrElse(event, doNotHandle))
        process(remainingEvents ++ newEvents)
    }
  }

  private def doNotHandle(value: Any) = Seq.empty[Any]
}
