package eu.pijusn.talk.builder.a_imperative

import org.scalatest.FlatSpec

case class Pizza(name: String,
                 toppings: Seq[String],
                 dough: Dough,
                 size: PizzaSize)

class PizzaBuilder {

  private var name: String = "Margarita"
  private var toppings = Seq.empty[String]
  private var dough: Dough = White
  private var size: PizzaSize = Small

  def setName(name: String): PizzaBuilder = {
    this.name = name
    this
  }

  def setToppings(toppings: Seq[String]): PizzaBuilder = {
    this.toppings = toppings
    this
  }

  def setDough(dough: Dough): PizzaBuilder = {
    this.dough = dough
    this
  }

  def setSize(size: PizzaSize): PizzaBuilder = {
    this.size = size
    this
  }

  def build(): Pizza = {
    require(toppings.nonEmpty, "no toppings")

    Pizza(
      this.name,
      this.toppings,
      this.dough,
      this.size
    )
  }
}

sealed trait Dough
case object Full extends Dough
case object White extends Dough
case object SourDough extends Dough

sealed trait PizzaSize
case object Small extends PizzaSize
case object Medium extends PizzaSize
case object Large extends PizzaSize

class PizzaTest extends FlatSpec {
  "Builder" should "construct pizza" in {
    val pizza = new PizzaBuilder()
      .setDough(Full)
      .setName("Hawaiian")
      .setToppings(Seq("Anchovy", "olives"))
      .setSize(Large)
      .build()

    println(pizza)
  }

  it should "construct pizza dynamically" in {
    val isGift = Math.random() < 0.2
    val builder = new PizzaBuilder()
      .setName("Pepperoni")
      .setToppings(Seq("Pepperoni", "Cheese", "Tomato Sauce"))

    if (isGift) builder.setSize(Large)

    val pizza = builder.build()

    println(pizza)
  }

  it should "not build pizza without toppings" in {
    def buildInvalidPizza = new PizzaBuilder()
      .setDough(Full)
      .setName("Hawaiian")
      .setSize(Large)
      .build()

    assertThrows[IllegalStateException](buildInvalidPizza)
  }
}
