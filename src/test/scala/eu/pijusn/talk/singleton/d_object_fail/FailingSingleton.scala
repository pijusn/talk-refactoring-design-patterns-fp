package eu.pijusn.talk.singleton.d_object_fail

import org.scalatest.FlatSpec

object FailingSingleton {
  throw new IllegalStateException

  def valueOf(n: Int): Int = n
}

class FailingSingleton extends FlatSpec {
  "FailingSingleton" should "throws ExceptionInInitializerError when used for the first time" in {
    assertThrows[ExceptionInInitializerError](FailingSingleton.valueOf(5))
  }

  it should "throws NoClassDefFoundError when used for second time" in {
    assertThrows[NoClassDefFoundError](FailingSingleton.valueOf(5))
  }
}
