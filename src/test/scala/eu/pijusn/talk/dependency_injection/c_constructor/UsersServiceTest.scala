package eu.pijusn.talk.dependency_injection.c_constructor

import org.scalatest.FlatSpec

class UsersServiceTest extends FlatSpec {
  "Creating user" should "accept new user" in {
    val validator = new Validator
    val service = new UsersService(validator)

    val userWithInvalidEmail = User("user@wix.com", "password")
    service.createUser(userWithInvalidEmail)
  }

  it should "validate email" in {
    val validator = new Validator
    val service = new UsersService(validator)

    val userWithInvalidEmail = User("bad-bad-email", "password")
    assertThrows[IllegalArgumentException](service.createUser(userWithInvalidEmail))
  }

  it should "validate password" in {
    val validator = new Validator
    val service = new UsersService(validator)

    val userWithInvalidEmail = User("user@wix.com", "!#$%^&*")
    assertThrows[IllegalArgumentException](service.createUser(userWithInvalidEmail))
  }
}
