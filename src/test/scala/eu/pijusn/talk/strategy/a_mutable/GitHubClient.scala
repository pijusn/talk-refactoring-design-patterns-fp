package eu.pijusn.talk.strategy.a_mutable

import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.util.EntityUtils

// TODO !!!!

class GitHubClient(driver: HttpDriver) {
  def getFile(organisation: String, repository: String, branch: String, path: String): String = {
    val url = s"https://github.com/$organisation/$repository/blob/$branch$path"
    driver.doGet(url)
  }
}

trait HttpDriver {
  def doGet(url: String): String
}

class ApacheHttpClient extends HttpDriver{
  override def doGet(url: String): String = {
    val client = HttpClientBuilder.create().build()
    val request = new HttpGet(url)
    val response = client.execute(request)
    EntityUtils.toString(response.getEntity)
  }
}
