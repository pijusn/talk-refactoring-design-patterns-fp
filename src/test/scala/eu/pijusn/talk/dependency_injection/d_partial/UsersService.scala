package eu.pijusn.talk.dependency_injection.d_partial

case class User(email: String, password: String)

object UsersService {
  def createUser(validator: Validator)(user: User): Unit = {
    if (!validator.isValidEmail(user.email)) throw new IllegalArgumentException("Invalid email")
    if (!validator.isValidPassword(user.password)) throw new IllegalArgumentException("Invalid password")

    // User is valid and should be created...
  }
}

class Validator {
  def isValidEmail(str: String): Boolean = str matches ".+@.+"
  def isValidPassword(str: String): Boolean = str matches "[a-zA-Z0-9]*"
}
