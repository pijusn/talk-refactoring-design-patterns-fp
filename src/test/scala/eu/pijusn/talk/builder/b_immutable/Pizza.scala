package eu.pijusn.talk.builder.b_immutable

import org.scalatest.FlatSpec

case class Pizza(name: String,
                 toppings: Seq[String],
                 dough: Dough,
                 size: PizzaSize)

case class PizzaBuilder(name: String = "Margarita",
                        toppings: Seq[String] = Seq.empty[String],
                        dough: Dough = White,
                        size: PizzaSize = Small
                       ) {

  def withName(name: String): PizzaBuilder = {
    this.copy(name = name)
  }

  def withTopping(toppings: Seq[String]): PizzaBuilder = {
    this.copy(toppings = toppings)
  }

  def withDough(dough: Dough): PizzaBuilder = {
    this.copy(dough = dough)
  }

  def withSize(size: PizzaSize): PizzaBuilder = {
    this.copy(size = size)
  }

  def build(): Pizza = {
    require(toppings.nonEmpty, "no toppings")

    Pizza(
      this.name,
      this.toppings,
      this.dough,
      this.size
    )
  }
}

sealed trait Dough
case object Full extends Dough
case object White extends Dough
case object SourDough extends Dough

sealed trait PizzaSize
case object Small extends PizzaSize
case object Medium extends PizzaSize
case object Large extends PizzaSize

class PizzaTest extends FlatSpec {
  "Builder" should "construct pizza" in {
    val pizza = new PizzaBuilder()
      .withDough(Full)
      .withName("Hawaiian")
      .withTopping(Seq("Anchovy", "olives"))
      .withSize(Large)
      .build()

    println(pizza)
  }

  it should "construct pizza dynamically" in {
    val isGift = Math.random() < 0.2
    var builder = new PizzaBuilder()
      .withName("Pepperoni")
      .withTopping(Seq("Pepperoni", "Cheese", "Tomato Sauce"))

    if (isGift) builder = builder.withSize(Large)

    val pizza = builder.build()

    println(pizza)
  }

  it should "not build pizza without toppings" in {
    def buildInvalidPizza = new PizzaBuilder()
      .withDough(Full)
      .withName("Hawaiian")
      .withSize(Large)
      .build()

    assertThrows[IllegalStateException](buildInvalidPizza)
  }
}
