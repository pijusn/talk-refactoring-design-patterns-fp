package eu.pijusn.talk.singleton.c_lazy_declarative

import eu.pijusn.talk.singleton.c_lazy_declarative.Factorial.factorial
import org.scalatest.FlatSpec

import scala.annotation.tailrec
import scala.collection.mutable

class Factorial private {
  private val cache = new mutable.HashMap[Long, BigInt]

  def of(n: Long): BigInt = cache.getOrElseUpdate(n, computedValueFor(n))

  @tailrec
  private def computedValueFor(n: Long, multiplier: BigInt = 1): BigInt = {
    n match {
      case 0 => multiplier * 1
      case x => computedValueFor(x - 1, multiplier * x)
    }
  }
}

object Factorial {
  lazy val factorial: Factorial = new Factorial
}

class FactorialTest extends FlatSpec {
  "Factorial" should "compute value of 5 factorial" in {
    val result = factorial.of(5)
    assert(result == 120)
  }

  it should "compute value of 30 factorial" in {
    val result = factorial.of(30)
    assert(result == BigInt("265252859812191058636308480000000"))
  }
}
