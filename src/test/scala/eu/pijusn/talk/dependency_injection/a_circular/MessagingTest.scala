package eu.pijusn.talk.dependency_injection.a_circular

import org.scalatest.FlatSpec

class MessagingTest extends FlatSpec {
  "Countdown" should "print time down" in {
    val messaging = new Messaging
    val handler = new MessageHandler

    messaging.handler = handler
    handler.messaging = messaging

    messaging.dispatch("countdown", 3)

    Thread.sleep(4200L)
  }
}
