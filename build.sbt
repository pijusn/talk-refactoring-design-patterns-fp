name := "talk-refactoring-design-patterns-fp"

version := "0.1"

scalaVersion := "2.12.3"

libraryDependencies += "com.google.guava" % "guava" % "23.0"
libraryDependencies += "org.apache.httpcomponents" % "httpclient" % "4.5.3"
libraryDependencies += "org.scalatest" % "scalatest_2.12" % "3.0.4" % "test"
libraryDependencies += "org.mockito" % "mockito-all" % "1.10.19" % "test"

