package eu.pijusn.talk.observers.c_event_loop

import java.time.{Duration, Instant}

import eu.pijusn.talk.observers.c_event_loop.Announcer.welcomeUser
import eu.pijusn.talk.observers.c_event_loop.ReturningUserService.asReturningUser
import eu.pijusn.talk.observers.c_event_loop.UserLoginService.asLoggedInUser
import org.scalatest.FlatSpec
import org.scalatest.Matchers._

// Just the data!
case class User(name: String, lastLogin: Instant)

// Business logic without side-effects
object UserLoginService {
  def asLoggedInUser(user: User, timestamp: Instant): UserLoggedIn = UserLoggedIn(
    previousLogin = user.lastLogin,
    user = user.copy(lastLogin = timestamp)
  )
}

object ReturningUserService {
  def asReturningUser(user: User, previousLogin: Instant): Option[UserReturned] = {
    val timeDelta = Duration.between(previousLogin, user.lastLogin)
    val hasLoggedInRecently = timeDelta.minusDays(7).isNegative
    if (hasLoggedInRecently) {
      None
    } else {
      Some(UserReturned(user))
    }
  }
}

// Side-effects you cannot avoid.
object Announcer {
  def welcomeUser(user: User): Unit = {
    println(s"Welcome back, ${user.name}. We missed you!")
  }
}

sealed trait Event
case class UserLoggedIn(user: User, previousLogin: Instant) extends Event
case class UserReturned(user: User) extends Event

sealed trait Request
case class LoginRequest(user: User) extends Request

class ServiceFacade {
  private val handle = new EventLoop({
    case LoginRequest(user) =>
      Seq(asLoggedInUser(user, Instant.now()))

    case UserLoggedIn(user, previousLogin) =>
      asReturningUser(user, previousLogin).toSeq

    case UserReturned(user) =>
      welcomeUser(user)
      Seq.empty
  })

  def login(user: User): Unit = {
    handle(LoginRequest(user))
  }
}

class UserLoginServiceTest extends FlatSpec {
  "logging in user" should "set last login timestamp" in {
    val oldTimestamp = Instant.ofEpochMilli(1400000000)
    val newTimestamp = Instant.ofEpochMilli(1500000000)
    val user = User("user", oldTimestamp)

    val result = asLoggedInUser(user, newTimestamp)

    result.user.lastLogin should be(newTimestamp)
  }

  it should "return previous login timestamp" in {
    val oldTimestamp = Instant.ofEpochMilli(1400000000)
    val newTimestamp = Instant.ofEpochSecond(1500000000)
    val user = User("user", oldTimestamp)

    val result = asLoggedInUser(user, newTimestamp)

    result.previousLogin should be(oldTimestamp)
  }
}

class ReturningUserService extends FlatSpec {
  "returning user" should "be one that has not logged in 14 days" in {
    val timestamp = Instant.ofEpochSecond(1500000000)
    val user = User("user", timestamp)

    val result = asReturningUser(user, daysBefore(timestamp, 14))
    result should contain(UserReturned(user))
  }

  "returning user" should "not be one that has recently logged in" in {
    val timestamp = Instant.ofEpochSecond(1500000000)
    val user = User("user", timestamp)

    val result = asReturningUser(user, daysBefore(timestamp, 1))
    result should be(empty)
  }

  private def daysBefore(instant: Instant, numberOfDays: Int) = instant.minus(Duration.ofDays(numberOfDays))
}

