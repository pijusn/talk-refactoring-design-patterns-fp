package eu.pijusn.talk.factory_method.b_declarative

import org.scalatest.FlatSpec

class DataFileTest extends FlatSpec {
  "Provider" should "support reading files over HTTPS" in {
    val file = DataFile("https://raw.githubusercontent.com/scala/scala/2.12.x/README.md")
    val content = file.content
    assert(content contains "Welcome!")
  }

  it should "support reading resource files" in {
    val file = DataFile("classpath:///smiley.txt")
    val content = file.content
    assert(content contains ":-)")
  }
}
