package eu.pijusn.talk.factory_method.a_imperative

import org.scalatest.FlatSpec

class DataFileTest extends FlatSpec {
  "Provider" should "support reading files over HTTPS" in {
    val provider = ContentProvider("https://raw.githubusercontent.com/scala/scala/2.12.x/README.md")
    val content = provider.fetch()
    assert(content contains "Welcome!")
  }

  it should "support reading resource files" in {
    val provider = ContentProvider("classpath:///smiley.txt")
    val content = provider.fetch()
    assert(content contains ":-)")
  }
}
