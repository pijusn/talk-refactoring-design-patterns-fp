package eu.pijusn.talk.dependency_injection.a_circular

import java.util.concurrent.Executors.newSingleThreadExecutor

class Messaging {
  private val executor = newSingleThreadExecutor()
  var handler: MessageHandler = _

  def dispatch(kind: String, message: Any): Unit = {
    executor.submit(asRunnable {
      handler.handle(kind, message)
    })
  }

  private def asRunnable(execute: => Unit) = new Runnable {
    override def run(): Unit = execute
  }
}

class MessageHandler {
  var messaging: Messaging = _

  def handle(kind: String, message: Any): Unit = {
    (kind, message) match {
      case ("countdown", 0) =>
        println("PARTY!!!1")

      case ("countdown", seconds: Int) =>
        Thread.sleep(1000L)
        println(s"$seconds...")
        messaging.dispatch("countdown", seconds - 1)

      case _ =>
        throw new IllegalStateException(s"kind=$kind; message=$message")
    }
  }
}
