package eu.pijusn.talk.chain_of_responsibility.a_imperative

import eu.pijusn.talk.chain_of_responsibility.a_imperative.Calculator._
import org.scalatest.FlatSpec

sealed trait Operation
case class Addition(a: Int, b: Int) extends Operation
case class Subtraction(a: Int, b: Int) extends Operation
case class Multiplication(a: Int, b: Int) extends Operation
case class Division(a: Int, b: Int) extends Operation

trait Calculator {
  private var next: Calculator = _

  def process(operation: Operation): Int

  def setNext(calculator: Calculator): Unit = synchronized {
    next = calculator
  }

  protected def callNext(operation: Operation): Int = synchronized {
    if (next == null) throw new IllegalArgumentException
    next.process(operation)
  }
}

object Calculator {
  class AdditionProcessor extends Calculator {
    override def process(operation: Operation): Int = operation match {
      case Addition(left, right) => left + right
      case _ => callNext(operation)
    }
  }

  class SubtractionProcessor extends Calculator {
    override def process(operation: Operation): Int = operation match {
      case Subtraction(left, right) => left - right
      case _ => callNext(operation)
    }
  }

  class MultiplicationProcessor extends Calculator {
    override def process(operation: Operation): Int = operation match {
      case Multiplication(left, right) => left * right
      case _ => callNext(operation)
    }
  }

  class DivisionProcessor extends Calculator {
    override def process(operation: Operation): Int = operation match {
      case Division(left, right) => left / right
      case _ => callNext(operation)
    }
  }
}

class CalculatorTest extends FlatSpec {
  "Calculator" should "add" in {
    val result = calculator.process(Addition(1, 3))
    assert(4 === result)
  }

  it should "subtract" in {
    val result = calculator.process(Subtraction(1, 3))
    assert(-2 === result)
  }

  it should "multiply" in {
    val result = calculator.process(Multiplication(1, 3))
    assert(3 === result)
  }

  it should "divide" in {
    val result = calculator.process(Division(1, 3))
    assert(0 === result)
  }

  private def calculator = {
    val p0 = new AdditionProcessor()
    val p1 = new SubtractionProcessor()
    val p2 = new MultiplicationProcessor()
    val p3 = new DivisionProcessor()

    p0.setNext(p1)
    p1.setNext(p2)
    p2.setNext(p3)

    p0
  }
}
