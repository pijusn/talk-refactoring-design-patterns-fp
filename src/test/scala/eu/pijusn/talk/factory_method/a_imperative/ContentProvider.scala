package eu.pijusn.talk.factory_method.a_imperative

import java.io.{File, FileReader, InputStreamReader}
import java.net.{URI, URL}

import com.google.common.io.CharStreams

trait ContentProvider {
  def fetch(): String
}

object ContentProvider {
  def apply(uri: String): ContentProvider = apply(new URI(uri))

  def apply(uri: URI): ContentProvider = {
    uri.getScheme match {
      case "https" => new RemoteFile(uri.getHost, uri.getPath)
      case "classpath" => new ResourceFile(uri.getPath)
      case scheme => throw new UnsupportedOperationException(s"scheme=$scheme")
    }
  }
}

class RemoteFile(host: String, path: String) extends ContentProvider {
  override def fetch(): String = {
    val url = new URL("https", host, path).openConnection().getInputStream
    val reader = new InputStreamReader(url)
    CharStreams.toString(reader)
  }
}

class ResourceFile(path: String) extends ContentProvider {
  override def fetch(): String = {
    val file = new File(getClass.getResource(path).getFile)
    val reader = new FileReader(file)
    CharStreams.toString(reader)
  }
}
